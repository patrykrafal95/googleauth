﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using GoogleAuth.Data;
using GoogleAuth.Models;
using GoogleAuth.Services;
using Microsoft.AspNetCore.Authentication;
using System.Security.Claims;

namespace GoogleAuth
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            #region auth
            services.AddAuthentication().AddGoogle(_ =>
            {
                _.ClientId = Configuration["Authentication:Google:ClientId"];
                _.ClientSecret = Configuration["Authentication:Google:ClientSecret"];
                _.UserInformationEndpoint = "https://www.googleapis.com/oauth2/v2/userinfo";
                _.ClaimActions.Clear();
                _.ClaimActions.MapJsonKey(ClaimTypes.NameIdentifier, "id");
                _.ClaimActions.MapJsonKey(ClaimTypes.Name, "name");
                _.ClaimActions.MapJsonKey(ClaimTypes.GivenName, "given_name");
                _.ClaimActions.MapJsonKey(ClaimTypes.Surname, "family_name");
                _.ClaimActions.MapJsonKey("urn:google:profile", "link");
                _.ClaimActions.MapJsonKey(ClaimTypes.Email, "email");
            });
            #endregion

            // Add application services.
            services.AddTransient<IEmailSender, EmailSender>();

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
