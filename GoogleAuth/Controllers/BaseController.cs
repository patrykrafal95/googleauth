﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NLog;

namespace GoogleAuth.Controllers
{
    public class BaseController : Controller
    {
        private readonly Logger _logger = null;

        public BaseController()
        {
            this._logger = LogManager.GetLogger(this.GetType().Name);
            this._logger.Info(this);
        }
    }
}